ReadMe for Functional Testing

1.) Make sure you're in the "FunctionalTest" folder
2.) Open up command window and run the selenium-server by typing java -jar selenium-server-standalone-2.44.0.jar
3.) Navigate inside the webdriverio-test folder

4.) To run a test type node <testFileName>. For example, node Login.js

5.) Do this for each test file inside the folder