/**
 * Created by Agustin on 11/18/2014.
 * Test for creating a new account
 */
var webdriverio = require('webdriverio');
var options = {
    desiredCapabilities: {
        browserName: 'firefox'
    }
};
webdriverio
    .remote(options)
    .init()
    .url('http://localhost:3000/')
    .click('[name="CreateAccount"]')
    .pause(3000)

    .setValue('[name="username"]','Administrator')
    .setValue('[name="useremail"]','Admin@spiralevents.com')
    .pause(1000)
    .setValue('[name="password"]','password')
    .setValue('[name="passwordconfirm"]','password')
    .click('input[type="checkbox"]')
    .pause(1000)
    //dont want to click create account because then we will not be able to use it again
    .click('input[value="Create account"]')
.end()