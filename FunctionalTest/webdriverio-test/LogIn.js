/**
 * Created by Agustin on 11/18/2014.
 * Test to see if the user can log in with correct password and user name
 *
 */
    //succesful
var webdriverio = require('webdriverio');
var options = {
    desiredCapabilities: {
        browserName: 'firefox'
    }
};
webdriverio
    .remote(options)
    .init()
    .url('http://localhost:3000/')
    //have to go to the html file and see what the label was called
    .click('input[type="checkbox"]')


    .click('[name="username"]')

    .setValue('[name="username"]','Admin@spiralevents.com')

    .click('[name="password"]')
    .setValue('[name="password"]','password')

    .click('input[value="Log in"]')
    .pause(5000)

.end()