/**
 * Created by Agustin on 11/19/2014.
 * Test to see if we can make events dynamically
 */
var webdriverio = require('webdriverio');
var options = {
    desiredCapabilities: {
        browserName: 'firefox'
    }
};
//NOT DONE YET STILL NEED TO UPLOAD FILE FOR PICTURE AND TO CCHANGE AM/PM
//created a variable so we can just name the diffrent events instead of looking at the code
var name="Angularjs Workshop";
//location=location
var location="Silicon Valley";
//date=date
var date="Dec 5";
//Start hour=Shour
var Shour="7";
//StartMinute-Smin
var Smin="30";
//to chose am/pm=Sampm
var Sampm="p"
//End hour =Ehour
var Ehour="9";
//End minute=Emin
var Emin="00";
//end of am or pm=Eampm
var Eampm="p";
//The cost is =cost
var cost="8.00";
//the description of the event is =description
var description="Meet Google's Angularjs team and learn how to develop a webapp with its client-side MVC model.";
webdriverio
    .remote(options)
    .init()
    .url('http://localhost:3000/eventlist')
    .setValue('[name="eventname"]',name)
    .setValue('[name="location"]',location)
    .setValue('[name="date"]',date)
    .setValue('[name="timestart_h"]',Shour)
    .setValue('[name="timestart_m"]',Smin)
    //.setValue('[name="timestart_ampm"]',Sampm)
   // .click('[name="cost"]')
    .setValue('[name="timeend_h"]',Ehour)
    .setValue('[name="timeend_m"]',Emin)
   // .setValue('[name="timeend_ampm"]',Eampm)
    .setValue('[name="cost"]',cost)
    .setValue('[name="description"]',description)

    .click('[name="submit"]')
    .pause(5000)
.end()