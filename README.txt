Getting this working
-----------------------
Install Node.js (from http://nodejs.org/)
Install MongoDB with default settings
Run mongodStart.bat to start the database daemon and start the website
The app is set to listen on port 3000, so navigate to http://localhost:3000 to test the app
To run functional tests, open the "FunctionalTest" folder. There is a ReadMe there with further instructions