// mongo-init.js
var mongo = require('mongodb');
var monk = require('monk');

module.exports = function initMongo() {
    return {
        db: monk('localhost:27017/spiralevents'),
        mongo: mongo
    };
}