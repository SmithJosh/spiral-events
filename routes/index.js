var express = require('express');
var async = require('async');
var router = express.Router();
var database = require('../mongo-init')();

var isAuthenticated = function (req, res, next) {
  // if user is authenticated in the session, call the next() to call the next request handler
  // Passport adds this method to request object. A middleware is allowed to add properties to
  // request and response objects
  var atLoginPage = req.path === "/" || req.path === "/signup";

  if (req.isAuthenticated()) {
    if (atLoginPage)
      res.redirect('/home');
    return next();
  }

  //If the user is at the login/signup page, don't redirect
  if (atLoginPage) return next();

  // If an anonymous user is trying to access a protected page, redirect to login
  res.redirect('/');
}

module.exports = function(passport){

  /* GET login page. */
  router.get('/', isAuthenticated, function(req, res) {
    // Display the Login page with any flash message, if any
    res.render('login', { message: req.flash('message') });
  });

  /* Handle Login POST */
  router.post('/login', passport.authenticate('login', {
    successRedirect: '/home',
    failureRedirect: '/',
    failureFlash : true
  }));

  /* GET Registration Page */
  router.get('/signup', isAuthenticated, function(req, res){
    res.render('register',{message: req.flash('message')});
  });

  /* Handle Registration POST */
  router.post('/signup', passport.authenticate('signup', {
    successRedirect: '/home',
    failureRedirect: '/signup',
    failureFlash : true
  }));

  /* GET Home Page */
  router.get('/home', isAuthenticated, function(req, res){
    var db = req.db;
    var collection = db.get('eventlist');

    collection.find({},{}, function(err, docs) {
      //res.json(docs);
      res.render('eventlist', {
        "eventlist" : docs
      });
    });
  });

  /* GET User List Page */
  router.get('/userlist', isAuthenticated, function(req, res){
    var db = req.db;
    var collection = db.get('usercollection');

    collection.find({},{}, function(err, docs) {
      res.render('userlist', {
        "userlist" : docs
      });
    });
  });

  /* Handle Logout */
  router.get('/signout', function(req, res) {
    req.logout();
    res.redirect('/');
  });

 /* Add Event to User Collection */
  router.post('/buytickets', function(req, res) {
    var db = req.db;
    var collection = db.get('usercollection');

    var quantity = Number(req.body.quantity);
    var eventID = req.body.eventID;

    collection.findOne({ "_id" : req.user }, function(err, user) {
      if (err) {
        console.log('Error finding user: ' + err);
        return done(err);
      }
      var userEvents = user.userEvents;
      var eventExists = false;
      for (var i = 0; i < userEvents.length; i++) {
        if (userEvents[i].eventID === eventID) {  //If the user has already bought tickets for the event
          var ticketsOwned = Number(userEvents[i].quantity) + quantity;
          userEvents[i] = {
            quantity: ticketsOwned,
            eventID: eventID
          };
          collection.update(
              { _id: req.user },
              {
                $set: {
                  userEvents: userEvents
                }
              }
          );
          eventExists = true;
          break;
        }
      }
      if (!eventExists) {
        collection.update(
            {_id: req.user},
            {
              $push: {
                userEvents: {
                  quantity: quantity,
                  eventID: eventID
                }
              }
            }
        );
      }
    });

    res.location("eventinfo?id=" + req.body.eventID);
    //And forward to success page
    res.redirect("eventinfo?id=" + req.body.eventID);
  });

  /* GET Add Event Page */
  router.get('/eventlist', function(req, res) {
    var db = req.db;
    var mongo = req.mongo;
    var collection = db.get('eventlist');
    console.log(db);

    collection.find({},{}, function(err, docs) {
      res.render('addevent', {
        "eventlist" : docs
      });
    });
  });
  
  /* Handle Add Event POST */
  router.post('/addevent', function(req, res) {
    var db = req.db;
    var collection = db.get('eventlist');

    var db = req.db;
    var collection = db.get('eventlist');
    var filename = req.body.picture;

    //Submit to the DBs
    collection.insert({
      "eventname" : req.body.eventname,
      "location" : req.body.location,
      "date" : req.body.date,
      "starttime" : req.body.timestart_h + ':' + req.body.timestart_m + ' ' + req.body.timestart_ampm,
      "endtime" : req.body.timeend_h + ':' + req.body.timeend_m + ' ' + req.body.timeend_ampm,
      "cost" : req.body.cost,
      "description" : req.body.description
    }, function (err, doc) {
      if (err) {
        console.log("Error adding the information to the database.");
        throw err;
      }
      else {
        console.log('Event added successfully');
      }
    });

    //If it worked, set the header so the address bar doesn't still say /delevent
    res.location("eventlist");
    //And forward to success page
    res.redirect("eventlist");
  });

  /* Handle Delete Event POST */
  router.post('/deleteevent', function(req, res) {
    var db = req.db;
    var collection = db.get('eventlist');
    console.log(req.body._id);
    //Delete from database
    collection.findOne({ "_id" : req.body._id }, function(err, user) {
      if (err){
        console.log('Error finding event: '+err);
        return done(err);
      }
      collection.remove(user);
    });

    //If it worked, set the header so the address bar doesn't still say /delevent
    res.location("eventlist");
    //And forward to success page
    res.redirect("eventlist");
  });

  /* GET Event Info Page */
  router.get('/eventinfo', isAuthenticated, function(req, res) {
    var db = req.db;
    var collection = db.get('eventlist');

    collection.findOne({ "_id" : req.query.id }, function(err, info) {
      if (err){
        console.log('Error finding event: '+err);
        return done(err);
      }
      //res.json(info);
      res.render('eventinfo', {
        "event" : info
      });
    });
  });

  /* GET User List Page */
  router.get('/profile', isAuthenticated, function(req, res) {
    var db = req.db;
    var eventcollection = db.get('eventlist');
    var usercollection = db.get('usercollection');

    usercollection.findOne({ "_id" : req.user }, function(err, info) {
      if (err){
        console.log('Error finding user: '+err);
        return done(err);
      }
      var siteUser = info;
      var userEvents = info.userEvents;
      var eventArray = new Array();

      async.each(userEvents, function(entry, callback) {
            eventcollection.findOne({"_id": entry.eventID}, function (err, event) {
              event.quantity = entry.quantity;
              eventArray.push(event);
              callback();
            });
          }, function(err) {
            res.render('profile', {
              "user" : siteUser,
              "eventlist" : eventArray
            });
          }
      );
    });
  });

  /* Handle Update Profile POST */
  router.post('/updateprofile', function(req, res) {
    var db = req.db;
    var collection = db.get('usercollection');

    var newusername = req.body.username;
    var newemail = req.body.useremail;

    collection.update(
        { _id: req.user },
        {
          $set: {
            username: newusername,
            email: newemail
          }
        }
    );

    //If it worked, set the header so the address bar doesn't still say /delevent
    res.location("profile");
    //And forward to success page
    res.redirect("profile");

  });

  return router;
}