var express = require('express');
var router = express.Router();

/* GET eventlist. */
router.get('/addevent', function(req, res) {
  var db = req.db;
  db.collection('eventlist').find().toArray(function (err, items) {
    res.json(items);
  });
});

module.exports = router;
