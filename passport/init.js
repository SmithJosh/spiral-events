var login = require('./login');
var signup = require('./signup');

module.exports = function(passport){

	// Passport needs to be able to serialize and deserialize users to support persistent login sessions
    passport.serializeUser(function(user, done) {
        console.log('serializing user: ');console.log(user);
        done(null, user._id);
    });

    passport.deserializeUser(function(user, done) {
        // Uses the entire user object instead of just the user id.
        // In order to deserialize with just the id, need to find a
        // different way to pass database object. Maybe create a db
        // class and require it?
        done(null, user);
    });

    // Setting up Passport Strategies for Login and SignUp/Registration
    login(passport);
    signup(passport);

}