var LocalStrategy   = require('passport-local').Strategy;
var bCrypt = require('bcryptjs');
var crypto = require('./crypto');
var flash = require('flash');

module.exports = function(passport){

	passport.use('signup', new LocalStrategy({
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, username, password, done) {

            //Set internal DB variable
            var db = req.db;

            //Get out form values. These rely on the "name" attributes
            var userName = req.body.username;
            var userEmail = req.body.useremail;
            var password = req.body.password;
            var passwordConfirm = req.body.passwordconfirm;
            var userAgreed = req.body.userAgreedTOS == "ON";

            //Declare array of events for each user
            var userEvents = new Array();

            //Set our collection
            var collection = db.get('usercollection');

            findOrCreateUser = function(){
                // find a user in Mongo with provided username
                collection.findOne({ 'username' :  userName }, function(err, user) {
                    // In case of any error, return using the done method
                    if (err){
                        console.log('Error in SignUp: '+err);
                        return done(err);
                    }
                    // already exists
                    if (user) {
                        console.log('User already exists with username: '+userName);
                        return done(null, false, req.flash('message','User Already Exists'));
                    } else {
                        // if there is no user with that username
                        // check if form was filled correctly
                        if (password === passwordConfirm && userAgreed === true && !isBlank(userName) && !isBlank(userEmail) && !isBlank(password) && !isBlank(passwordConfirm)) {
                            //Hash password
                            crypto.cryptPassword(password, function(error, hash) {
                                if (error) res.send("There was a problem hashing your password.");
                                else {

                                    //Submit to the DBs
                                    collection.insert({
                                        "username" : userName,
                                        "email" : userEmail,
                                        "password" : hash,
                                        "userEvents": userEvents

                                    }, function (err, doc) {
                                        if (err) {
                                            console.log("Error adding the information to the database.");
                                            throw err;
                                        }
                                        else {
                                            console.log('User Registration Successful');
                                            return done(null, user);
                                        }
                                    });
                                }
                            });
                        }
                        else {
                            console.log('Form not filled correctly!');
                            return done(null, false, req.flash('message','Please fill form correctly'));
                        }
                    }
                });
            };
            // Delay the execution of findOrCreateUser and execute the method
            // in the next tick of the event loop
            process.nextTick(findOrCreateUser);
        })
    );

    // Generates hash using bcrypt
    var createHash = function(password) {
        return bcrpt.hash(password, bCrypt.genSaltSync(10), null);
    }

    //Returns true if string is blank, null, or undefined
    function isBlank(str) {
        return (!str || /^\s*$/.test(str));
    }
}