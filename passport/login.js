var LocalStrategy  = require('passport-local').Strategy;
var bCrypt = require('bcryptjs');
var crypto = require('./crypto');

module.exports = function(passport){

	passport.use('login', new LocalStrategy({
            passReqToCallback : true
        },
        function(req, username, password, done) {
            //Set internal DB variable
            var db = req.db;

            //Get out form values. These rely on the "name" attributes
            var userName = req.body.username;
            var password = req.body.password;

            //Set our collection
            var collection = db.get('usercollection');

            // check in mongo if a user with username exists or not
            collection.findOne({ $or: [ { 'username' : userName }, { 'email' : userName } ] },
                function(err, user) {
                    // In case of any error, return using the done method
                    if (err)
                        return done(err);
                    // Username does not exist, log the error and redirect back
                    if (!user){
                        console.log('User Not Found with username '+userName);
                        return done(null, false, req.flash('message', 'User Not found.'));                 
                    }
                    crypto.comparePassword(password, user.password, function(err, isPasswordMatch) {
                        if (err) {
                            console.log("There was a problem hashing the password.");
                            return done(err);
                        }
                        else {
                            if (isPasswordMatch) {
                                return done(null, user);
                            }
                            else {
                                console.log('Invalid Password');
                                return done(null, false, req.flash('message', 'Invalid Password')); // redirect back to login page
                            }
                        }
                    });
                }
            );

        })
    );

    var isValidPassword = function(user, password){
        return bCrypt.compareSync(password, user.password);
    }
    
}